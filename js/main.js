(function() {
  var closeModal = document.querySelector(".modal-button");
  var modalWindow = document.getElementById("js-modal");
  
  closeModal.addEventListener("click", function(event) {
    event.preventDefault();
    modalWindow.classList.add("modal-window-close");
  });
})();

(function() {
  var view = {
    displayMessage: function(msg) {
      var message = document.getElementById("message");
      message.innerHTML = msg;
    },
    displayHit: function(location) {
      var cell = document.getElementById(location);
      cell.setAttribute("class", "hit");
    },
    displayMiss: function(location) {
      var cell = document.getElementById(location);
      cell.setAttribute("class", "miss");
    }
  };

  var model = {
    boardSize: 7,
    numShips: 3,
    shipLength: 3,
    shipsSunk: 0,
    ships: [ { locations: [0, 0, 0], hits: ["", "", ""] },
             { locations: [0, 0, 0], hits: ["", "", ""] },
             { locations: [0, 0, 0], hits: ["", "", ""] } 
           ],
    fire: function(guess) {
      for (var i = 0; i < this.numShips; i++) {
        var ship = this.ships[i];
        var index = ship.locations.indexOf(guess);
        if (index >= 0) {
          ship.hits[index] = "hit";
          view.displayHit(guess);
          view.displayMessage("Попал, епта!");
          
          if (this.isSunk(ship)) {
            view.displayMessage("Ааа, ты потопил мой корабль!");
            this.shipsSunk++;
          }
          return true;
        }
      }
      view.displayMiss(guess);
      view.displayMessage("Хагага, промахнулся!");
      return false;
    },
    isSunk: function(ship) {
      for (var i = 0; i < this.shipLength; i++) {
        if (ship.hits[i] !== "hit") {
          return false;
        }
      }
      return true;
    },
    generateShipLocations: function() {
      var locations;
      for (var i = 0; i < this.numShips; i++) {
        do {
          locations = this.generateShip();
        } while (this.collision(locations));
        this.ships[i].locations = locations;
      }
    },
    generateShip: function() {
      var direction = Math.floor(Math.random() * 2);
      var row, col;
      
      if (direction === 1) {
        row = Math.floor(Math.random() * this.boardSize);
        col = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
      } else {
        row = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
        col = Math.floor(Math.random() * this.boardSize);
      }
      
      var newShipLocations = [];
      for (var i = 0; i < this.shipLength; i++) {
        if (direction === 1) {
          newShipLocations.push(row + "" + (col + i));
        } else {
          newShipLocations.push((row + i) + "" + col);
        }
      }
      return newShipLocations;
    },
    collision: function(locations) {
      for (var i = 0; i < this.numShips; i++) {
        var ship = this.ships[i];
        for (var j = 0; j < locations.length; j++) {
          if (ship.locations.indexOf(locations[j]) >= 0) {
            return true;
          }
        }
      }
      return false;
    }
  };

  var controller = {
    guesses: 0,
    processGuess: function(guess) {
      var location = parseGuess(guess);
      if (location) {
        this.guesses++;
        var hit = model.fire(location);
        if (hit && model.shipsSunk === model.numShips) {
          view.displayMessage(" Бляяяять, ты уничтожил мой флот за " + this.guesses + " выстрелов!!! ");
        }
      }
    }
  };

  var alphabet = ["a", "b", "c", "d", "e", "f", "g"];

  function parseGuess(guess) {
    if (guess === null || guess.length !== 2) {
      alert("Упс, введите верные координаты выстрела.");
    } else {
      firstChar = guess.charAt(0);
      var row = alphabet.indexOf(firstChar);
      var column = guess.charAt(1);

      if (isNaN(row) || isNaN(column)) {
        alert("Ой, введены не верные символы.");
      } else if (row < 0 || row >= model.boaedsize || column < 0 || column >= model.boardSize) {
        alert("Ой, координаты находятся за пределами игрового поля.");
      } else {
        return row + column;
      } 
    }
    return null;
  }

  function init() {
    var fireButton = document.getElementById("fireButton");
    fireButton.onclick = handleFireButton;
    var guessInput = document.getElementById("guessInput");
    guessInput.onkeypress = handleKeyPress;
  }

  function handleFireButton() {
    var guessInput = document.getElementById("guessInput");
    var guess = guessInput.value;
    controller.processGuess(guess);
    guessInput.value = "";
    
    model.generateShipLocations();
  }

  function handleKeyPress(e) {
    var fireButton = document.getElementById("fireButton");
    if (e.keyCode === 13) {
      fireButton.click();
      return false;
    }
  }
  window.onload = init;
})();